# MASTERMIND #

### Instrucciones para la realización y entrega del minijuego ###

Desarrolle un juego tipo "**Mastermind**".

El juego deberá contar, al menos, con una  **pantalla de presentación**, una **pantalla de records**, la **pantalla de juego** y una **pantalla de créditos**.
 
Preste especial atención al desarrollo y diseño del mismo. Es preferible **menor funcionalidad bien terminada** que incorporar un **mayor número de opciones sin finalizar adecuadamente**.
 
Le entrega consistirá en un archivo de texto que contendrá:
 
* **Autores**: El nombre del autor (o autores si se ha realizado en parejas). 
* **Fuentes**: La dirección del repositorio donde se puedan descargar los fuentes. Si el repositorio no es público, se indicará la dirección de algún servicio de almacenamietno (Google Drive, Dropbox, Mega...) desde donde se pueda descargar un archivo .tar.gz con todos los fuentes. 
* **Binarios (Ejecutables)**: La dirección de algún servicio de almacenamietno (Google Drive, Dropbox, Mega...) desde donde se pueda descargar un archivo .tar.gz con el ejecutable para Linux x64 y Windows x64. Idealmente, se deberán proporcionar los dos binarios. Si no es posible entregar los dos ejecutables, será imprescidible que al menos el juego compile y funcione correctamente en GNU/Linux. 
* **Vídeo**: La dirección de algún servicio (Youtube, Vimeo...) donde se pueda ver un breve vídeo (1 ó 2 minutos) del proyecto en funcionamiento. 
* **Otra información**: A continuación se incluirá la información que se considere oportuna sobre el modo de ejecución del juego, controles, etc...
 
Si el juego ha sido desarrollado en grupos de 2 personas, cada integrante del equipo subirá el mismo archivo de texto en esta tarea.