/*
 * ScoreBoardLogic.h
 *
 *  Created on: 8 dic. 2016
 *      Author: joe
 */

#ifndef INCLUDE_SCOREBOARDLOGIC_H_
#define INCLUDE_SCOREBOARDLOGIC_H_
#include <vector>
#include <string>

#define N 10  /* máximo número de puntuaciones a mostrar */

class ScoreBoardLogic {

  public:
	ScoreBoardLogic();
    ~ScoreBoardLogic();

    void addNewScore(std::string name, int points);

    std::vector < std::pair <std::string, int> > loadScores();

    void showScoreBoard();


  private:
    std::vector < std::pair <std::string, int> > score;

    // Constructor de copia, si lo usa se quejará
    ScoreBoardLogic(const ScoreBoardLogic&);
};

#endif /* INCLUDE_SCOREBOARDLOGIC_H_ */
