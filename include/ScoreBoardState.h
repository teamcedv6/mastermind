/*
 * ScoreBoard.h
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */

#ifndef INCLUDE_SCOREBOARDSTATE_H_
#define INCLUDE_SCOREBOARDSTATE_H_

#include <Ogre.h>
#include <OgreOverlay.h>
#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreSingleton.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "IntroState.h"
#include "DrawEntities.h"
#include "CameraAnimation.h"
#include "ScoreBoardLogic.h"
#include "MessageState.h"

class ScoreBoardState : public Ogre::Singleton<ScoreBoardState>, public GameState
{
public:
	ScoreBoardState() {}

	// Gestion de estados
	void enter();
	void exit();
	void pause();
	void resume();

	// Gestion de eventos de teclado y ratón
	void keyPressed (const OIS::KeyEvent &e);
	void keyReleased (const OIS::KeyEvent &e);
	void mouseMoved (const OIS::MouseEvent &e);
	void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
	void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);
	void WriteToTextureT(const String &str, TexturePtr destTexture, Image::Box destRectangle, Font* font, const ColourValue &color, char justify /*= 'l'*/,  bool wordwrap /*= true*/);

	// Gestion de eventos antes y después de renderizar fames
	bool frameStarted (const Ogre::FrameEvent& evt );
	bool frameEnded (const Ogre::FrameEvent& evt);

	// Camino de instaciar objetos heredado de OGRE::Singleton
	static ScoreBoardState& getSingleton();
	static ScoreBoardState* getSingletonPtr();

protected:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneMgr;
	Ogre::Viewport* viewport;
	Ogre::Camera* _camera;

	Real _animTime;
	CEGUI::Window* _window;

	Ogre::RaySceneQuery* _raySceneQuery;
	DrawEntities* _drawEntities;
	ScoreBoardLogic _scb;

	bool _exitGame;

};


#endif /* INCLUDE_SCOREBOARDSTATE_H_ */
