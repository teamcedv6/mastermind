#ifndef MASTERMIND_LOGIC_H
#define MASTERMIND_LOGIC_H

#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include "Colours.h"

using namespace std;

class MasterMindLogic {

public:
	struct TestResult {
		int colourMatches;
		int exactMatches;
	};

	MasterMindLogic();
	MasterMindLogic(const int rowNumber, const int combinatioLenght);
	MasterMindLogic(MasterMindLogic & masterMindLogic);
	~MasterMindLogic();
	MasterMindLogic& operator = (const MasterMindLogic &mMLogic);
	bool testCombination(std::vector<Colours::Colour *> *combination, TestResult * result);
	void generateCombination();

	const int getCombinationLenght() const;
	std::vector<Colours::Colour *> * getCombination() const;
	const int getCurrentRow() const;
	const int getRowNumber() const;
	const bool hasLost() const;
	int generateScore ();

	TestResult * _testResult;

private:
	std::vector<Colours::Colour *> * _combination;
	std::vector<int> * _coloursNumber;
	int _rowNumber;
	int _combinationLenght;
	int _currentRow;
	int _score;

};

#endif
