/*********************************************************************
 * Minijuego 1 - Mastermind - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 *
 * Para estructurar el juego se utilizará un esquema basado en estados
 * proporcionado por David Vallejo Fernández en M1.13 El bucle de juego
 *
 * GameStates
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * M1.13 El bucle de juego
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef GameManager_H
#define GameManager_H

#include <stack>
#include <Ogre.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include <OIS/OIS.h>

#include "InputManager.h"
#include "TrackManager.h"
#include "SoundFXManager.h"


using namespace Ogre;
using namespace std;

class GameState;

class GameManager : public Ogre::FrameListener, public Ogre::Singleton<GameManager>, public OIS::KeyListener, public OIS::MouseListener
{
 public:
  GameManager ();
  ~GameManager (); // Limpieza de todos los estados.

  // Para el estado inicial.
  void start (GameState* state);
  
  // Funcionalidad para transiciones de estados.
  void changeState (GameState* state);
  void pushState (GameState* state);
  void popState ();
  
  // Heredados de Ogre::Singleton.
  static GameManager& getSingleton ();
  static GameManager* getSingletonPtr ();

  TrackPtr getTrackPtr (int sound);
  SoundFXPtr getSoundFXPtr(int fx);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneManager;
  Ogre::RenderWindow* _renderWindow;

  // Funciones de configuración.
  void loadResources ();
  bool configure ();
  bool _initSDL();
  void loadMusicFx();
  
  // Heredados de FrameListener.
  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

 private:
  // Funciones para delegar eventos de teclado
  // y ratón en el estado actual.
  bool keyPressed (const OIS::KeyEvent &e);
  bool keyReleased (const OIS::KeyEvent &e);

  bool mouseMoved (const OIS::MouseEvent &e);
  bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  
  void initializeCegui();

  // Gestor de eventos de entrada.
  InputManager *_inputMgr;
  // Estados del juego.
  std::stack<GameState*> _states;

	// Manejadores del sonido.
  	TrackManager* _pTrackManager;
    SoundFXManager* _pSoundFXManager;
	TrackPtr _mainTrack;
	SoundFXPtr _fxWin;
	SoundFXPtr _fxLose;
	SoundFXPtr _fxSelect;
	SoundFXPtr _fxSet;
	SoundFXPtr _fxCheck;

};

#endif
