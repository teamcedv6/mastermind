/*
 * AuthorState.h
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */

#ifndef INCLUDE_AUTHORSTATE_H_
#define INCLUDE_AUTHORSTATE_H_

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "IntroState.h"
#include "DrawEntities.h"
#include "CameraAnimation.h"

class AuthorState : public Ogre::Singleton<AuthorState>, public GameState
{
public:
	AuthorState() {}

	// Gestion de estados
	void enter();
	void exit();
	void pause();
	void resume();

	// Gestion de eventos de teclado y ratón
	void keyPressed (const OIS::KeyEvent &e);
	void keyReleased (const OIS::KeyEvent &e);
	void mouseMoved (const OIS::MouseEvent &e);
	void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
	void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

	// Gestion de eventos antes y después de renderizar fames
	bool frameStarted (const Ogre::FrameEvent& evt );
	bool frameEnded (const Ogre::FrameEvent& evt);

	// Camino de instaciar objetos heredado de OGRE::Singleton
	static AuthorState& getSingleton();
	static AuthorState* getSingletonPtr();

protected:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneMgr;
	Ogre::Viewport* viewport;
	Ogre::Camera* _camera;

	// Rayo de seleccion
	Ogre::RaySceneQuery* _raySceneQuery;

	// Dibujar el nodo
	DrawEntities* _drawEntities;

	bool _exitGame;
};
#endif /* INCLUDE_AUTHORSTATE_H_ */
