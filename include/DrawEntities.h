/*
 * DrawEntities.h
 *
 *  Created on: 7 dic. 2016
 *      Author: joe
 */

#ifndef INCLUDE_DRAWENTITIES_H_
#define INCLUDE_DRAWENTITIES_H_
#include <Ogre.h>
#include <OIS/OIS.h>
#include <vector>

#include "Colours.h"
//#include "ScoreBoardLogic.h"

#include <vector>

#define NOSELECTABLE 1 << 0  // Mascara para objetos no seleccionables
#define BALLFREE     1 << 1  // Mascara para seleccion de bolas
#define HITZONE      1 << 2  // Mascara para zonas calientes de tablero
#define CHECKBUTTON  1 << 3	// Mascara para boton de chequear fila
#define MENUBUTTONS  1 << 4  // Mascara botones de menu
#define EXITBUTTONS  1 << 5  // Máscara para botón salida de scores y author


using namespace Ogre;
using namespace std;


class DrawEntities
{
  private:
	SceneNode * _colourSelected;
	// Posiciones entre las que se puede mover la bola seleccionada
	Real _x_o, _x_f, _y_o, _y_f;
	float _width, _height;

	void cleanBoardRecursively(SceneManager* sceneManager, SceneNode* node);

  public:
    DrawEntities();
    ~DrawEntities();

    void showCombination(std::vector<Colours::Colour*>* combination,SceneManager* sceneManager);
    void showSelected(int posx, int posy, int relx, int rely);
    void drawBoard(int rowNumber,SceneManager* sceneManager);
    void cleanBoard(SceneManager* sceneManager);
    void drawCombination(SceneManager* sceneManager);
    void showButton(int rowNumber,SceneManager* sceneManager);
    void hideButton(SceneManager* sceneManager);
    void drawTestResult(int rowNumber,int colourMatches,int exactMatches,SceneManager* sceneManager);
    void setColourInBoard(SceneManager* sceneManager, Colours::Colour * c, int rowNumber, int position);
    void drawAuthor(SceneManager* sceneManager);
    void drawIntro(SceneManager* sceneManager);
    void drawRoom(SceneManager* sceneManager);
    void drawScoreBoard(SceneManager* sceneManager);
    void setColourSelected(SceneManager* sceneManager, Colours::Colour * c, const Vector3 & position, const Vector3 & rayDirection);
    void emptyColourSelected(SceneManager* sceneManager);
    void setBallSelectedPossibleCoordinates(Real x_o, Real x_f, Real y_o, Real y_f, int widh, int height);
};
#endif /* INCLUDE_DRAWENTITIES_H_ */
