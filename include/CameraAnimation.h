/*
 * CameraAnimation.h
 *
 *  Created on: Dec 4, 2016
 *      Author: root
 */

#ifndef INCLUDE_CAMERAANIMATION_H_
#define INCLUDE_CAMERAANIMATION_H_

#include <Ogre.h>

using namespace Ogre;

class CameraAnimation : Ogre::FrameListener {
 public:
	CameraAnimation();
	CameraAnimation(Camera * cam, Root * root, Vector3 & position, Vector3 & lookAt, Real time);
	CameraAnimation(Camera * cam, Root * root, Real time);
	~CameraAnimation();
	void startAnimation();
	bool hasFinished();

	CameraAnimation* setPosition(Vector3 & position);
	CameraAnimation* setLookAt(Vector3 & lookAt);
	CameraAnimation* setFOV(Radian fov);
	CameraAnimation* setOrientation(Quaternion & quaternion);

	bool frameStarted(const FrameEvent& evt);
	bool frameEnded(const FrameEvent& evt);

 private:
	Camera * _cam;
	Root * _root;

	bool _position;
	bool _lookAt;
	bool _fov;
	bool _orientation;
	bool _hasFinished;

	Vector3 * _currentPosition;
	Vector3 * _positionChange;
	Vector3 * _initialPosition;

	Vector3 *  _currentLookAt;
	Vector3 * _initialLookAt;
	Vector3 * _lookAtChange;

	Ogre::Radian _currentfov;
	Ogre::Radian _initialfov;
	Ogre::Radian _fovChange;

	Quaternion _orientationQuaternion;
	Quaternion _initialOrientation;

	Real _t;
	Real _elapsedTime;

	void easeFunctionVector(Vector3 * currentValue, Vector3 * startValue, Vector3 * valueChange);
	void easeFunctionFloat(float * currentValue, float startValue, float valueChange);
	void easeFunctionRadian(Radian * currentValue, Radian startValue, Radian valueChange);
};


#endif /* INCLUDE_CAMERAANIMATION_H_ */
