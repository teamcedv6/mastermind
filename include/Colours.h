/*
 * Colours.h
 *
 *  Created on: Nov 27, 2016
 *      Author: root
 */

#ifndef COLOURS_H_
#define COLOURS_H_

#include <string>
#include <vector>

using namespace std;

class Colours {
public:

	struct Colour {
	    int index;
	    string colorValue;
	};

	static std::vector<Colour*>* getPossibleColours();
	static int getNumberOfColours();


private:
	static std::vector<Colour*> * _possibleColours;
	static void initializeColours();
	Colours();
	~Colours();

};

#endif /* COLOURS_H_ */
