/*********************************************************************
 * Minijuego 1 - Mastermind - Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 *
 * Para estructurar el juego se utilizará un esquema basado en estados
 * proporcionado por David Vallejo Fernández en M1.13 El bucle de juego
 *
 * GameStates
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * M1.13 El bucle de juego
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef MessageState_H
#define MessageState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <stdlib.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"
#include "PlayState.h"
#include "DrawEntities.h"
#include "ScoreBoardLogic.h"

class MessageState : public Ogre::Singleton<MessageState>, public GameState
{
 public:
  MessageState () { }

  enum MessageType {
	  WIN=0,
	  LOSE=1,
	  LEVEL=2
  };

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

  void setType(MessageType type);

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static MessageState& getSingleton ();
  static MessageState* getSingletonPtr ();

 protected:

  MessageType _type;
  CEGUI::OgreRenderer* _renderer;
  DrawEntities * _drawEntities;
  ScoreBoardLogic* _scoreBoardLogic;
  CEGUI::Window* _window;

  void createWinGUI();
  void createLoseGUI();
  void createLevelGUI();

  void menuButtonCallback(const CEGUI::EventArgs &e);
  void saveRecordCallback(const CEGUI::EventArgs &e);
  void selectLevelCallback1(const CEGUI::EventArgs &e);
  void selectLevelCallback2(const CEGUI::EventArgs &e);
  void selectLevelCallback3(const CEGUI::EventArgs &e);

};

#endif
