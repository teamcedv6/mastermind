#include "IntroState.h"
#include "PlayState.h"
#include "AuthorState.h"
#include "ScoreBoardState.h"
#include <CEGUI.h>

#include "MessageState.h"


template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
   _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");

  // Inicialización de DrawEntities
  _drawEntities = new DrawEntities();
  // Pintamos la habitación
  _drawEntities->drawRoom(_sceneMgr);


  //Inicializacion de ray query
  _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

	// IntroState
	Vector3 p1 = Vector3(-20.26677,96.51058,30.79758);
	Vector3 lookAt1 = Vector3(-25.99988,34.0,0.0);
	_enterAnim = new CameraAnimation(_camera, _root, p1, lookAt1, 5.0);
	_enterAnim-> setFOV(Radian(Degree(15))) -> startAnimation();

  _exitGame = false;
}

void
IntroState::exit()
{

}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
	// IntroState
	Vector3 p1 = Vector3(-20.26677,96.51058,30.79758);
	Vector3 lookAt1 = Vector3(-25.99988,34.0,0.0);
	_enterAnim = new CameraAnimation(_camera, _root, p1, lookAt1, 5.0);
	_enterAnim-> setFOV(Radian(Degree(15))) -> startAnimation();
}

void
IntroState::author()
{
}

void
IntroState::score()
{
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{

}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx,rely);
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	// No procesa clicks hasta que la animación no esté terminada.
	if(!_enterAnim->hasFinished()) return;

	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	Ogre::uint32 mask;

	if(id == OIS::MB_Left) {
		mask = MENUBUTTONS;

		Ogre::Ray r = setRayQuery(_raySceneQuery, _root->getAutoCreatedWindow(),_camera, posx, posy, mask);
		Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();

		Ogre::RaySceneQueryResult::iterator it;
		it = result.begin();

		if (it != result.end()) {
			if(it->movable->getName() == "IntroPlay"){
				/*Ogre::LogManager::getSingletonPtr()->logMessage("*** IntroPlay push ***");
				pushState(PlayState::getSingletonPtr());*/
				// Mostrar la selección de dificultad
				MessageState::getSingletonPtr()->setType(MessageState::LEVEL);
				changeState(MessageState::getSingletonPtr());
			}
			else if(it->movable->getName() == "IntroAuthor"){
				Ogre::LogManager::getSingletonPtr()->logMessage("*** IntroAuthor push ***");
				pushState(AuthorState::getSingletonPtr());
			}
			else if(it->movable->getName() == "IntroScoreboard"){
				Ogre::LogManager::getSingletonPtr()->logMessage("*** IntroScoreboard push ***");
				pushState(ScoreBoardState::getSingletonPtr());
			}
			else if(it->movable->getName() == "IntroExit"){
				Ogre::LogManager::getSingletonPtr()->logMessage("*** IntroExit push ***");
				popState();
			}
		}
	}
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

IntroState*
IntroState::getSingletonPtr ()
{
return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
