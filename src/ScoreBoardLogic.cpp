/*
 * ScoreBoardLogic.cpp
 *
 *  Created on: 8 dic. 2016
 *      Author: joe
 */
#include "ScoreBoardLogic.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

ScoreBoardLogic::ScoreBoardLogic() {
	this->score=vector < pair <string, int> > (N);
	// this->score=vector < pair <string, int> > ((N), make_pair("CEDV 6ª Edition", 1000));
}

void ScoreBoardLogic::addNewScore(string name, int points)
{
  if ( points >= score[N-1].second )	{
	// Siempre saldrá el último
	score.pop_back();

	// Metemos al record nuevo al final
	score.push_back(make_pair(name, points));

	// auxiliar
	vector < pair<string, int> > aux(1, make_pair("",0));

  	// Ordenamos por burbuja
  	for (int i=0; i<(score.size()-1); i++) {
  	  for (int j=i+1; j<(score.size()); j++) {
  		  if (score[j].second > score[i].second) {
  			  aux[0]=score[j];
  			  score[j]=score[i];
  			  score[i]=aux[0];
  		  }
  	  }
  	}
  	// Escribimos fichero a disco
  	ofstream rec("scores.txt");
  	if ( rec.fail() )
	  cerr << ("No se puede escribir el fichero de records a disco.") << endl;

  	vector < pair <string, int> > :: iterator it=score.begin();

  	for(; it!= score.end(); ++it)
  	  rec << it->first << ";" << it->second << ";";

  	rec.close();
  }
}

vector < pair <string, int> > ScoreBoardLogic::loadScores()
{
  // Cargamos del fichero de disco
  ifstream in("scores.txt");
  string buffer;

  if ( in.fail() )
	  cerr << ("No se puede escribir el fichero de records a disco.") << endl;


  // Recorremos el vector y lo vamos completando desde fichero
  for ( int i=0; i<score.size(); i++ ) {
    getline(in, buffer,';');
    score[i].first=buffer;
    getline(in, buffer, ';');
    score[i].second=atoi(buffer.c_str());
  }
  in.seekg(0, ios::beg);
  in.close();

  return score;
}

void ScoreBoardLogic::showScoreBoard()
{
  vector < pair <string, int> > :: iterator it=score.begin();

  for(; it!= score.end(); ++it) {
    cout << it->first << "---" << it->second << endl;
  }

}



ScoreBoardLogic::~ScoreBoardLogic()
{
}



