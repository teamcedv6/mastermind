/*
 * Colours.cpp
 *
 *  Created on: Nov 27, 2016
 *      Author: root
 */

#include "Colours.h"

std::vector<Colours::Colour*>* Colours::_possibleColours = new std::vector<Colours::Colour*>();

std::vector<Colours::Colour *> * Colours::getPossibleColours() {
	if(_possibleColours->size() == 0) {
		initializeColours();
	}

	return _possibleColours;
}

int Colours::getNumberOfColours() {
	if(_possibleColours->size() == 0) {
			initializeColours();
	}

	return _possibleColours->size();
}

void Colours::initializeColours() {
	_possibleColours->reserve(5);
	Colours::Colour c0 = {0, "Red"};
	Colours::Colour * pc0 = new Colours::Colour;
	*pc0 = c0;
	_possibleColours->push_back(pc0);

	Colours::Colour c1 = {1, "Blue"};
	Colours::Colour * pc1 = new Colours::Colour;
	*pc1 = c1;
	_possibleColours->push_back(pc1);

	Colours::Colour c2 = {2, "Cyan"};
	Colours::Colour * pc2 = new Colours::Colour;
	*pc2 = c2;
	_possibleColours->push_back(pc2);

	Colours::Colour c3 = {3, "Green"};
	Colours::Colour * pc3 = new Colours::Colour;
	*pc3 = c3;
	_possibleColours->push_back(pc3);

	Colours::Colour c4 = {4, "Magenta"};
	Colours::Colour * pc4 = new Colours::Colour;
	*pc4 = c4;
	_possibleColours->push_back(pc4);

	Colours::Colour c5 = {5, "Yellow"};
	Colours::Colour * pc5 = new Colours::Colour;
	*pc5 = c5;
	_possibleColours->push_back(pc5);
}
