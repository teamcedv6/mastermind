#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <Ogre.h>

#include "GameManager.h"
#include "GameState.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager ()
{
  _root = 0;
  _initSDL();
}

GameManager::~GameManager ()
{
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  if (_root)
    delete _root;
}

void
GameManager::start
(GameState* state)
{
  // Creación del objeto Ogre::Root.
  _root = new Ogre::Root();
  _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
  //_sceneManager->addRenderQueueListener(new Ogre::OverlaySystem());

  _pTrackManager = OGRE_NEW TrackManager;
    _pSoundFXManager = OGRE_NEW SoundFXManager;

  loadResources();
  loadMusicFx();

  // Cargar música
  _mainTrack->play();

  if (!configure())
    return;    

  _inputMgr = new InputManager;
  _inputMgr->initialise(_renderWindow);

  // Registro como key y mouse listener...
  _inputMgr->addKeyListener(this, "GameManager");
  _inputMgr->addMouseListener(this, "GameManager");

  // El GameManager es un FrameListener.
  _root->addFrameListener(this);


  Ogre::Camera *_camera = _sceneManager->createCamera("IntroCamera");
  Ogre::Viewport * _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  _viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
  //InputManager::getSingletonPtr()->getMouse()->getMouseState().X.

  _camera->setPosition(62.60238, 45.43774, 62.71519);
  _camera->lookAt(0.000001, 34.0, 0.0);
  _camera->setFOVy(Ogre::Radian(Ogre::Degree(70.0)));
  _camera->setNearClipDistance(0.1);
  _camera->setFarClipDistance(300.0);

  /* Creamos un punto de luz
  SceneNode* lightNode = _sceneManager->createSceneNode("lightNode");
  _sceneManager->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
  Light* lightEntity = _sceneManager->createLight("lightEntity");
  lightEntity->setType(Light::LT_SPOTLIGHT);
  lightEntity->setPosition(11.27606, 98.2641, 64.05135);
  lightEntity->setDirection(Vector3(0.000001, 34.0, 0.0));
  lightEntity->setSpotlightInnerAngle(Degree(5.0));
  lightEntity->setSpotlightOuterAngle(Degree(55.0));
  lightEntity->setSpotlightFalloff(0.0f);
  lightNode->attachObject(lightEntity);
  _sceneManager->getRootSceneNode()->addChild(lightNode);*/

  Light* light = _sceneManager->createLight("Light1");
    light->setType(Light::LT_DIRECTIONAL);
    light->setDirection(Vector3(-1,0,0));

    Light* light2 = _sceneManager->createLight("Light2");
      light2->setType(Light::LT_POINT);
      light2->setPosition(37.66144, 71.91479, 2.49251);
      light2->setSpecularColour(0.9, 0.9, 0.9);
      light2->setDiffuseColour(0.9, 0.9, 0.9);

  // Creamos una luz de ambiente
      _sceneManager->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
  _sceneManager->setAmbientLight(Ogre::ColourValue(0.1, 0.1, 0.1));

  //createOverlay();
  initializeCegui();

  // Transición al estado inicial.
  changeState(state);

  // Bucle de rendering.
  _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    // exit() sobre el último estado.
    _states.top()->exit();
    // Elimina el último estado.
    _states.pop();
  }

  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
  // Pausa del estado actual.
  if (!_states.empty())
    _states.top()->pause();
  
  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::popState ()
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  // Vuelta al estado anterior.
  if (!_states.empty())
    _states.top()->resume();
}

void
GameManager::loadResources ()
{
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
}

bool
GameManager::configure ()
{
  if (!_root->restoreConfig()) {
    if (!_root->showConfigDialog()) {
      return false;
    }
  }
  _renderWindow = _root->initialise(true, "Mastermind - UCLM CEDV 6");
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
  return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{  
  assert(msSingleton);
  return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
  _inputMgr->capture();
  return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _states.top()->frameEnded(evt);
}

bool
GameManager::keyPressed 
(const OIS::KeyEvent &e)
{
  _states.top()->keyPressed(e);
  return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
  _states.top()->keyReleased(e);
  return true;
}

bool
GameManager::mouseMoved 
(const OIS::MouseEvent &e)
{
  _states.top()->mouseMoved(e);
  return true;
}

bool
GameManager::mousePressed 
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mousePressed(e, id);
  return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mouseReleased(e, id);
  return true;
}


void
GameManager::initializeCegui() {
	//CEGUI
	CEGUI::OgreRenderer::bootstrapSystem();
	CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
	CEGUI::Font::setDefaultResourceGroup("Fonts");
	CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

	CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");

	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

	// Let's make the OS and the CEGUI cursor be in the same place
	//CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition().
	CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);

	// load all the fonts (if they are not loaded yet)
	CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");

	//Sheet
	CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}

bool GameManager::_initSDL () {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);

  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }

  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);

  return true;
}

void
GameManager::loadMusicFx ()
{
	// Cargamos la música
	_mainTrack = _pTrackManager->load("music.ogg");

	// Cargamos los efectos de sonido
	_fxWin = _pSoundFXManager->load("win.ogg");
	_fxLose = _pSoundFXManager->load("lose.ogg");
	_fxSelect = _pSoundFXManager->load("select.ogg");
	_fxSet = _pSoundFXManager->load("set.ogg");
	_fxCheck = _pSoundFXManager->load("check.ogg");
}

TrackPtr GameManager::getTrackPtr (int sound) {

	switch(sound){
		case 1: return _mainTrack; break;
		default:return _mainTrack;
	}

}

SoundFXPtr GameManager::getSoundFXPtr (int fx) {
		switch(fx){
			case 1: return _fxWin; break;
			case 2: return _fxLose; break;
			case 3: return _fxSelect; break;
			case 4: return _fxSet; break;
			case 5: return _fxCheck; break;
			default:return _fxSelect;
		}
}
