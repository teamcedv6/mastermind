/*
 * AuthorState.cpp
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */
#include "AuthorState.h"
#include "IntroState.h"
#include "CameraAnimation.h"
#include <CEGUI.h>

template<> AuthorState* Ogre::Singleton<AuthorState>::msSingleton = 0;

void AuthorState::enter()
{
	_root=Ogre::Root::getSingletonPtr();

	// Recuperamos SceneManager y Cámara de GameManager
	_sceneMgr =_root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	
	// Cremos rayo de consulta
	_raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

	// Pintamos la pizarra
	Vector3 p4 = Vector3(11.20589,96.51058,20.73602);
	Vector3 lookAt4 = Vector3(0.0,80.0,-79.50001);
	CameraAnimation * anim4 = new CameraAnimation(_camera, _root, p4, lookAt4, 5.0);

	anim4-> setFOV(Radian(Degree(50))) -> startAnimation();
}

void AuthorState::exit()
{
}

void AuthorState::pause()
{
}

void AuthorState::resume()
{
}

void AuthorState::keyPressed(const OIS::KeyEvent &e)
{
}

void AuthorState::keyReleased(const OIS::KeyEvent &e)
{
	if (e.key == OIS::KC_ESCAPE)	{
		_exitGame=true;
	}
}

void AuthorState::mouseMoved(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx,rely);
}

void AuthorState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

		Ogre::uint32 mask;

		if(id == OIS::MB_Left) {
			mask = EXITBUTTONS;

			Ogre::Ray r = setRayQuery(_raySceneQuery, _root->getAutoCreatedWindow(),_camera, posx, posy, mask);
			Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();

			Ogre::RaySceneQueryResult::iterator it;
			it = result.begin();

			if (it != result.end()) {
				if(it->movable->getParentNode()->getName()=="AuthorExit")
					popState();
				}
		}
}

void AuthorState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool AuthorState::frameStarted (const Ogre::FrameEvent& e)
{
	return true;
}

bool AuthorState::frameEnded (const Ogre::FrameEvent & e)
{
	if (_exitGame)
		return false;

	return true;
}

AuthorState& AuthorState::getSingleton()
{
	assert(msSingleton);
	return *msSingleton;
}

AuthorState* AuthorState::getSingletonPtr()
{
	return msSingleton;
}





