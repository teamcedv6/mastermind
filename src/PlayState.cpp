#include "PlayState.h"
#include <CEGUI.h>
#include <MessageState.h>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _masterMindLogic = new MasterMindLogic(_rowNumber, 4);

  _combination = new std::vector<Colours::Colour*>(_masterMindLogic->getCombinationLenght());
  _selectedColour = NULL;
  _gameFinished = false;
  _scoreGame=0;

  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");


    // Inicialización de DrawEntities
    _drawEntities = new DrawEntities();


    if(_sceneMgr->hasSceneNode("Board")) {
    	_drawEntities->cleanBoard(_sceneMgr);
    }

    _drawEntities->drawBoard(_masterMindLogic->getRowNumber(),_sceneMgr);
	_drawEntities->drawCombination(_sceneMgr);

    _coloursLeft = _masterMindLogic->getCombinationLenght();
    _coordinatesCalculated = false;

    //Inicializacion de ray query
    _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

    // Animamos la cámara
  	// PlayState
  	Vector3 p = Vector3(0.0,100.0,0.0);
  	Vector3 lookAt = Vector3(0.0,34.0,0.0);
  	Quaternion q = Quaternion(1, -1, 0, 0);

  	/* Old animation
  	_enterAnimation = new CameraAnimation(_camera, _root, p, lookAt, 5.0);
  	_enterAnimation -> startAnimation();
  	*/
  	_enterAnimation = new CameraAnimation(_camera, _root, 3.0);
  	_enterAnimation->setPosition(p)->setOrientation(q);
  	_enterAnimation->setFOV(Radian(Degree(35)));
  	_enterAnimation -> startAnimation();

  	_exitGame = false;
}

void
PlayState::exit ()
{
}

void
PlayState::pause()
{
}

void
PlayState::resume()
{

}

void
PlayState::score()
{
}


bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
	if(_enterAnimation->hasFinished() && !_coordinatesCalculated){
		/* Calculamos con dos rayos las coordenadas de lo que se ve desde la camara en el plano XZ con altura en Y -44, de forma que
		 * usaremos estas coordenadas para poder pintar la bola en la misma posicion que el raton cuando se seleccione.
		 * */
		Ogre::Plane yPlane(Ogre::Vector3::UNIT_Y, 44);

		Vector3 pos_o;
		Vector3 pos_f;

		Ogre::Ray r = _camera->getCameraToViewportRay(0, 0);
		std::pair<bool, Real> point = r.intersects(yPlane);

		if (point.first)
		{
			pos_o = r.getPoint(point.second);
		}

		r = _camera->getCameraToViewportRay(1, 1);
		point = r.intersects(yPlane);


		if (point.first)
		{
			pos_f = r.getPoint(point.second);
		}

		_drawEntities->setBallSelectedPossibleCoordinates(pos_o.x, pos_f.x, pos_o.z, pos_f.z, _root->getAutoCreatedWindow()->getWidth(),  _root->getAutoCreatedWindow()->getHeight());
		_coordinatesCalculated = true;
	}

	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx,rely);
	_drawEntities->showSelected(posx, posy, relx, rely);
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	// Si la animacion de entrada no ha terminado, no procesamos el click (puede ocasionar problemas al seleccionar la bola, porque no se ha calculado el espacio de la pantalla
	if (!_enterAnimation->hasFinished() || _gameFinished) return;


	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	Ogre::uint32 mask;

	if(id == OIS::MB_Right) {

		mask = BALLFREE;

		Ogre::Ray r = setRayQuery(_raySceneQuery, _root->getAutoCreatedWindow(),_camera, posx, posy, mask);
		Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();

		Ogre::RaySceneQueryResult::iterator it;
		it = result.begin();

		if (it != result.end()) {
			GameManager::getSingletonPtr()->getSoundFXPtr(3)->play();
			// Select Ball Sound
			//GameManager::getSingletonPtr()->getSoundFXPtr(3)->play();
			// Si ha seleccionado una bola de color, recuperamos el color de la bola, la pintamos en pantalla y almacenamos el color seleccionado
			const string& name = it->movable->getName();
			Ogre::LogManager::getSingletonPtr()->logMessage("*** Ball selected ->" + name);
			int n = std::atoi(name.substr(5,5).c_str());
			std::vector<Colours::Colour*>* colours = Colours::getPossibleColours();
			_selectedColour = colours->at(n);

			_drawEntities->setColourSelected(_sceneMgr, _selectedColour, _sceneMgr->getSceneNode(name)->getPosition(),r.getDirection());
		}
		else {
			_drawEntities->emptyColourSelected(_sceneMgr);
		}
	}
	else if (id == OIS::MB_Left) {
		mask = HITZONE | CHECKBUTTON;

		Ogre::Ray r = setRayQuery(_raySceneQuery, _root->getAutoCreatedWindow(),_camera, posx, posy, mask);
				Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();

				Ogre::RaySceneQueryResult::iterator it;
				it = result.begin();

				if (it != result.end()) {
					if(it->movable->getQueryFlags() == HITZONE){
						GameManager::getSingletonPtr()->getSoundFXPtr(4)->play();
						const string& name = it->movable->getName();
						int row = std::atoi(name.substr(8,9).c_str());
						int y = std::atoi(name.substr(11,11).c_str());

						Ogre::LogManager::getSingletonPtr()->logMessage("***** selected " +  name + "position=" + name.substr(12,12));

						// Si ha pulsado en una zona caliente de la fila en la que esta jugando y tiene un color seleccionado
						if(row == _masterMindLogic->getCurrentRow() && _selectedColour != NULL){
							if(_combination->at(y) == NULL) _coloursLeft--;
							_combination->at(y) = _selectedColour;

							_drawEntities->setColourInBoard(_sceneMgr, _selectedColour, row, y);

							// Ya ha seleccionado todos los colores -> mostrar boton de check
							if(_coloursLeft == 0) {
								_drawEntities->emptyColourSelected(_sceneMgr);
								_drawEntities->showButton(row,_sceneMgr);
							}

						}

					}
					else if(it->movable->getQueryFlags() == CHECKBUTTON) {
						GameManager::getSingletonPtr()->getSoundFXPtr(5)->play();
						bool finished;
						MasterMindLogic::TestResult *result = new MasterMindLogic::TestResult;
						Ogre::LogManager::getSingletonPtr()->logMessage("*** Pre test");
						finished =_masterMindLogic->testCombination(_combination,result);
						Ogre::LogManager::getSingletonPtr()->logMessage("*** Post test");
						// Ha ganado
						if(finished){
							GameManager::getSingletonPtr()->getSoundFXPtr(1)->play();
							// Desactivamos la interacción
							_gameFinished = true;
							_drawEntities->showCombination(_combination,_sceneMgr);
							// Solo si gana calcula puntos
							_scoreGame=_masterMindLogic->generateScore();
							// Mostrar mensaje de victoria
							MessageState::getSingletonPtr()->setType(MessageState::WIN);
							changeState(MessageState::getSingletonPtr());
						}else{
							// No ha ganado
							// Ha perdido?
							if(_masterMindLogic->hasLost()){
								GameManager::getSingletonPtr()->getSoundFXPtr(2)->play();
								// Desactivamos la interacción
								_gameFinished = true;
								// Mostrar mensaje de derrota

								_scoreGame =_masterMindLogic->generateScore();
								//pushState(ScoreBoardState::getSingletonPtr());
								MessageState::getSingletonPtr()->setType(MessageState::LOSE);
								changeState(MessageState::getSingletonPtr());
							}
							// Sigue jugando
							else {
								_drawEntities->drawTestResult(_masterMindLogic->getCurrentRow()-1,result->colourMatches,result->exactMatches,_sceneMgr);
								_drawEntities->hideButton(_sceneMgr);
								_coloursLeft = _masterMindLogic->getCombinationLenght();
								delete _combination;
								_combination = new std::vector<Colours::Colour*>(_masterMindLogic->getCombinationLenght());
							}

						}
					}
				}
	}
}


void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PlayState::setRowNumber(int rowNumber) {
	_rowNumber = rowNumber;
}

const int PlayState::getScore() const {
	return _scoreGame;
}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
