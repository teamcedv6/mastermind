#include "MasterMindLogic.h"

#include <Ogre.h>
#include <stdlib.h>

MasterMindLogic::MasterMindLogic() {
	_currentRow = 1;
	_rowNumber = 10;
	_combinationLenght = 4;
	_coloursNumber = new vector<int>();
	_combination = new vector<Colours::Colour *>();
	generateCombination();
	_score=0;

	for(int i = 0; i < Colours::getNumberOfColours(); i++) {
		_coloursNumber->push_back(0);
	}

	_testResult->colourMatches = 0;
	_testResult->exactMatches = 0;
}

MasterMindLogic::MasterMindLogic(const int rowNumber, const int combinationLenght) : _rowNumber(rowNumber), _combinationLenght(combinationLenght) {
	_currentRow = 1;
	_coloursNumber = new vector<int>();
	_combination = new vector<Colours::Colour *>();

	for(int i = 0; i < Colours::getNumberOfColours(); i++) {
			_coloursNumber->push_back(0);
	}

	generateCombination();

}

MasterMindLogic::~MasterMindLogic() {
	delete _combination;
	delete _coloursNumber;
}

bool MasterMindLogic::testCombination(vector<Colours::Colour*> * combination, TestResult * result){
    Colours::Colour * point;
    vector<int> currentColourMatches = vector<int>(*_coloursNumber);
    vector<int> possibleColourMatches = vector<int>();

    for(int i = 0; i < Colours::getNumberOfColours(); i++) {
    		possibleColourMatches.push_back(0);
    }

    bool equals = true;

    std::ostringstream os;

    os << "N_colors " << currentColourMatches.at(0) << " " << currentColourMatches.at(1) << " " <<
    		currentColourMatches.at(2) << " " << currentColourMatches.at(3) << " " << currentColourMatches.at(4) << "\n";

    result -> colourMatches = 0;
    result -> exactMatches = 0;
    
    if(!hasLost()){
		for(unsigned int i = 0; i < combination->size(); i++) {
		  point = combination->at(i);

		  os<< "----- " << point-> colorValue << " == " << _combination->at(i)->colorValue << " ??";
		  if(point->colorValue == _combination->at(i)->colorValue) {
			  os << "Exact matched! ";
			  result -> exactMatches++;
			  currentColourMatches[point->index]--;
		  }
		  else {
			  equals = false;

			  if(currentColourMatches[point->index] > 0) {
				  possibleColourMatches[point->index]++;
				  os << "Colour matched! ";
			  }
			  else {
				  os << "No match.. ";
			  }
		  }
		}

		for(unsigned int i = 0; i < possibleColourMatches.size(); i++) {
			if(possibleColourMatches.at(i) <= currentColourMatches.at(i)) {
				result->colourMatches += possibleColourMatches.at(i);
			}
			else {
				result->colourMatches += currentColourMatches.at(i);
			}
		}

		os << "\n";

		_currentRow++;


		os << "combination = " << _combination->at(0)->colorValue << " " << _combination->at(1)->colorValue <<
				" " << _combination->at(2)->colorValue << " " << " " << _combination->at(3)->colorValue;
		os << " colour = " << result->colourMatches << " exact = " << result->exactMatches;
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Result -> " + os.str());
		return equals;
    }
    else {
    	return false;
    }

}

void MasterMindLogic::generateCombination() {
  vector<Colours::Colour *> * colours = Colours::getPossibleColours();
  int colourNumber = colours->size();
  int selectedIndex;
  
  ostringstream os;
  srand(time(NULL));
  for(int i = 0; i < _combinationLenght; i++) {
    selectedIndex = rand() % colourNumber;
    os << "selIndex " << selectedIndex;
    Colours::Colour * selectedColour = colours->at(selectedIndex);
    os << " selCol " << selectedColour->colorValue;
    _combination->push_back(selectedColour);
    os << " preColNum " << _coloursNumber->at(selectedColour->index);
    _coloursNumber->at(selectedColour->index) += 1;
    os << " postColNum " << _coloursNumber->at(selectedColour->index);
    os << "\n";
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Gen -> " + os.str());
    os.str("");

  }  
}

const int MasterMindLogic::getCombinationLenght() const {
	return _combinationLenght;
}

std::vector<Colours::Colour *> * MasterMindLogic::getCombination() const{
	return _combination;
}

const int MasterMindLogic::getCurrentRow() const {
	return _currentRow;
}

const int MasterMindLogic::getRowNumber() const {
	return _rowNumber;
}

const bool MasterMindLogic::hasLost() const {
	return _currentRow > _rowNumber;
}

int MasterMindLogic::generateScore() {
	_score = 0;

	switch (_rowNumber) {
			case 5:
				_score = ( (_currentRow-1) * 50 ) + 500;
				break;
			case 10:
				_score = ( (_currentRow-1) * 30 ) + 300;
				break;
			case 15:
				_score = ( (_currentRow-1) * 10 ) + 100;
	}
	return (_score );
}

