/*
 * DrawEntities.cpp
 *
 *  Created on: 7 dic. 2016
 *      Author: joe
 */
#include "DrawEntities.h"
#include "InputManager.h"

DrawEntities::DrawEntities()
{
	_colourSelected = NULL;
	_x_o = 0; _x_f = 100; _y_o = 0, _y_f = 100;
	_width = 800; _height = 600;
}

DrawEntities::~DrawEntities()
{

}

void DrawEntities::drawRoom(SceneManager* sceneManager)
{

	if(!sceneManager->hasSceneNode("Room")){

			SceneNode *room = sceneManager->createSceneNode("Room");

			// Creamos la geometria estatica del escenario
			StaticGeometry* stage = sceneManager->createStaticGeometry("Room");
			Entity* stageObj = sceneManager->createEntity("Room","Room.mesh");
			stage->addEntity(stageObj, Vector3(0,0,0));
			stage->build();  // Operacion para construir la geometria

			drawIntro(sceneManager);
			drawScoreBoard(sceneManager);
			drawAuthor(sceneManager);
			drawBoard(15,sceneManager);
		}

}

void DrawEntities::drawIntro(SceneManager* sceneManager)
{
	// Recuperamos el nodo del escenario

	SceneNode *room = sceneManager->getSceneNode("Room");
	sceneManager->getRootSceneNode()->addChild(room);
	room->setPosition(0,0,0);

	// Pintamos el botón IntroPlay
	SceneNode *nodeIntroPlay = sceneManager->createSceneNode("IntroPlay");
	Entity *entIntroPlay = sceneManager->createEntity("IntroPlay", "Play.mesh");
	nodeIntroPlay->attachObject(entIntroPlay);
	entIntroPlay->setQueryFlags(MENUBUTTONS);
	nodeIntroPlay->setPosition(-25.99988,34.0,-6);
	room->addChild(nodeIntroPlay);

	// Pintamos el botón IntroScoreboard
	SceneNode *nodeIntroScoreboard = sceneManager->createSceneNode("IntroScoreboard");
	Entity *entIntroScoreboard = sceneManager->createEntity("IntroScoreboard", "IntroScoreboard.mesh");
	nodeIntroScoreboard->attachObject(entIntroScoreboard);
	entIntroScoreboard->setQueryFlags(MENUBUTTONS);
	nodeIntroScoreboard->setPosition(-25.99988,34.0,-2);
	room->addChild(nodeIntroScoreboard);

	// Pintamos el botón IntroAuthor
	SceneNode *nodeIntroAuthor = sceneManager->createSceneNode("IntroAuthor");
	Entity *entIntroAuthor = sceneManager->createEntity("IntroAuthor", "IntroAuthor.mesh");
	nodeIntroAuthor->attachObject(entIntroAuthor);
	entIntroAuthor->setQueryFlags(MENUBUTTONS);
	nodeIntroAuthor->setPosition(-25.99988,34.0,2);
	room->addChild(nodeIntroAuthor);

	// Pintamos el botón IntroExit
	SceneNode *nodeIntroExit = sceneManager->createSceneNode("IntroExit");
	Entity *entIntroExit = sceneManager->createEntity("IntroExit", "IntroExit.mesh");
	nodeIntroExit->attachObject(entIntroExit);
	entIntroExit->setQueryFlags(MENUBUTTONS);
	nodeIntroExit->setPosition(-25.99988,34.0,6);
	room->addChild(nodeIntroExit);

}

void DrawEntities::showCombination(std::vector<Colours::Colour*>* combination,SceneManager* sceneManager)
{


	for (int x = 0; x < 4;x++){
		string material = combination->at(x)->colorValue;

		// Recuperamos la fila
		std::stringstream ballquestion;
		string baseBallquestion = "ball_";
		ballquestion << baseBallquestion << x ;
		Entity *ball= sceneManager->getEntity(ballquestion.str());
		ball->setMaterialName(material);

		ballquestion.str("");
	}
}

void DrawEntities::showSelected(int posx, int posy, int relx, int rely)
{
	if(_colourSelected != NULL) {
		_colourSelected->setPosition(_x_o + ((_x_f - _x_o) * (posx/_width)) , 44, _y_o + ((_y_f - _y_o) * (posy/_height)));
	}
}

void DrawEntities::drawBoard(int rowNumber, SceneManager* sceneManager)
{
	/*
	 *
	 * Room
	 * 		Board
	 * 			Head(Bolas solución)
	 * 			BaseBoard(numRow) 			--- BaseBoard_x
	 * 				HitZone(4) 				----HitZone_xx_y
	 * 					PlacedBall(4)		----PlacedBall_xx_y
	 * 				TestResult(1)			----TestResult_x
	 * 			Base(adorno parte inferior)
	 * 			SelectableBalls
	 * 				Ball(5)					----Ball_n
	 *
	 * SelectedBall			---- Cuelga directamente de root para establecer posiciones absolutas
	 *
	 * Posiciones
	 * Room(0,0,0)
	 * Board(0,34,-17)
	 * Head(0,0,0)
	 * BaseBoard(0,0,initBase)
	 * HitZone(initHitZone-4,1,0)
	 * TestResult(initHitZone-4,1,0)
	 */

	// Recuperamos el nodo del escenario - Su posición es (0,0,0)
	SceneNode *room = sceneManager->getSceneNode("Room");

	SceneNode *board = sceneManager->createSceneNode("Board");
	board->setPosition(0,34,-17);
	room->addChild(board);

	// Pintamos la cabeza del tablero
	SceneNode *nodeHead = sceneManager->createSceneNode("Head");
	Entity *entHead = sceneManager->createEntity("Head", "Head.mesh");
	nodeHead->attachObject(entHead);
	entHead->setQueryFlags(NOSELECTABLE);
	nodeHead->setPosition(0,0,0);
	board->addChild(nodeHead);

	std::stringstream nameNodeBase;
	string base = "BaseBoard_";
	int splitBase = 2;
	int initBase = 2;

	for(int x=1;x<=rowNumber;x++) {

		// Pintamos las filas
		nameNodeBase << base << x;
		Ogre::LogManager::getSingletonPtr()->logMessage("######Row#####" + nameNodeBase.str());
		SceneNode *nodeBaseBoard = sceneManager->createSceneNode(nameNodeBase.str());
		Entity *entBaseBoard = sceneManager->createEntity(nameNodeBase.str(), "Base.mesh");
		nodeBaseBoard->attachObject(entBaseBoard);
		entBaseBoard->setQueryFlags(NOSELECTABLE);
		nodeBaseBoard->setPosition(0,0,initBase);
		board->addChild(nodeBaseBoard);
		nameNodeBase.str("");
		initBase += splitBase;
		// Fin filas

		// Pintamos las zonas calientes
		std::stringstream nameNodeHitZone,nameNodeTestResult;
		string hitZone = "HitZone_"; string testResult = "TestResult_";
		int splitHitZone = 2;
		int initHitZone = 0;
		for(int y=0;y<4;y++) {
			if(x < 10)
				nameNodeHitZone << hitZone << "0" << x << "_" <<y;
			else
				nameNodeHitZone << hitZone << x << "_" <<y;
			Ogre::LogManager::getSingletonPtr()->logMessage("- " + nameNodeHitZone.str());
			SceneNode *nodeHitZone = sceneManager->createSceneNode(nameNodeHitZone.str());
			Entity *entHitZone = sceneManager->createEntity(nameNodeHitZone.str(), "HitZone.mesh");
			nodeHitZone->attachObject(entHitZone);
			entHitZone->setQueryFlags(HITZONE);
			nodeHitZone->setPosition(initHitZone-4,1,0);
			nodeBaseBoard->addChild(nodeHitZone);
			nameNodeHitZone.str("");
			initHitZone += splitHitZone;
		}
		// Fin zonas calientes

		// Pintamos las zonas de resultado
		nameNodeTestResult << testResult << x;
		Ogre::LogManager::getSingletonPtr()->logMessage("- " + nameNodeTestResult.str());
		SceneNode *nodeTestResult = sceneManager->createSceneNode(nameNodeTestResult.str());
		Entity *entTestResult = sceneManager->createEntity(nameNodeTestResult.str(), "TestResult.mesh");
		entTestResult->setQueryFlags(NOSELECTABLE);
		nodeTestResult->attachObject(entTestResult);
		nodeTestResult->setPosition(initHitZone-4,1,0);
		nodeBaseBoard->addChild(nodeTestResult);
		nameNodeTestResult.str("");
		// Fin zonas resultado

	}

	// Pintamos la base del tablero
	SceneNode *nodeBaseBoard = sceneManager->createSceneNode("BaseBoard");
	Entity *entBaseBoard = sceneManager->createEntity("BaseBoard", "Base.mesh");
	nodeBaseBoard->attachObject(entBaseBoard);
	entBaseBoard->setQueryFlags(NOSELECTABLE);
	nodeBaseBoard->setPosition(0,0,initBase);
	board->addChild(nodeBaseBoard);

	// Pintamos el espacio con las bolas elegibles
	SceneNode *selectableBallSpace = sceneManager->createSceneNode("SelectableBalls");
	board->addChild(selectableBallSpace);
	selectableBallSpace->translate(12, 0, 16, Node::TS_PARENT);

	SceneNode *selectableBall;
	Entity *selectableBallEntity;
	std::vector<Colours::Colour*> * colours = Colours::getPossibleColours();
	Colours::Colour * c;
	string * name;

	// Pintamos tantas bolas como colores haya
	for (int i = 0; i < colours->size(); i++) {
		c = colours->at(i);
		std::ostringstream 	s;
		s << c-> index;
		name = new string("Ball_" + s.str());
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Ball name =" + *name);

		selectableBall = sceneManager->createSceneNode(*name);
		selectableBallEntity = sceneManager->createEntity(*name,"Ball.mesh");
		selectableBallEntity->setMaterialName(c->colorValue,"General");
		selectableBall->attachObject(selectableBallEntity);
		selectableBallEntity->setQueryFlags(BALLFREE);
		selectableBallSpace->addChild(selectableBall);
		selectableBall->translate(0, 0, i*3, Node::TS_PARENT);
	}
}

void DrawEntities::cleanBoard(SceneManager* sceneManager){
	cleanBoardRecursively(sceneManager, sceneManager->getSceneNode("Board"));
}

void DrawEntities::cleanBoardRecursively(SceneManager* sceneManager, SceneNode* node){
	SceneNode::ChildNodeIterator it = node->getChildIterator();

	while(it.hasMoreElements()){
		SceneNode * sn = (SceneNode*)it.getNext();
		cleanBoardRecursively(sceneManager, sn);
	}

	SceneNode::ObjectIterator oi = node->getAttachedObjectIterator();
	while(oi.hasMoreElements()){
		sceneManager->destroyEntity(oi.getNext()->getName());
	}

	sceneManager->destroySceneNode(node);

}

void DrawEntities::drawTestResult(int rowNumber,int colourMatches,int exactMatches,SceneManager* sceneManager){

	int totalBalls,auxColourMatches,auxExactMatches;
	// IntroState
	std::vector<Vector3> positions;

	Vector3 result1 = Vector3(-0.5,0,-0.5); Vector3 result2 = Vector3(0.5,0,-0.5); Vector3 result3 = Vector3(-0.5,0,0.5); Vector3 result4 = Vector3(0.5,0,0.5);

	positions.push_back(result1); positions.push_back(result2); positions.push_back(result3); positions.push_back(result4);

	totalBalls = colourMatches + exactMatches - 1;
	// Recuperamos la fila
	std::stringstream nameTestResult;
	string baseResult = "TestResult_";
	nameTestResult << baseResult << rowNumber ;
	SceneNode *row= sceneManager->getSceneNode(nameTestResult.str());

	std::stringstream nameNodeResultBall;
	string resultBall = "ResultBall_";


	for(int x=0;x<colourMatches;x++) {

			// Pintamos las zonas de resultado
			nameNodeResultBall << resultBall << rowNumber  << "_" << totalBalls;
			Ogre::LogManager::getSingletonPtr()->logMessage("- " + nameNodeResultBall.str());
			SceneNode *nodeBall = sceneManager->createSceneNode(nameNodeResultBall.str());
			Entity *entHitZone = sceneManager->createEntity(nameNodeResultBall.str(), "Ball.mesh");
			entHitZone->setMaterialName("White");
			entHitZone->setQueryFlags(NOSELECTABLE);
			nodeBall->attachObject(entHitZone);
			nodeBall->scale(0.5,0.5,0.5);
			nodeBall->setPosition(positions.at(totalBalls));
			row->addChild(nodeBall);
			nameNodeResultBall.str("");
			// Fin zonas resultado
			totalBalls--;

	}
	for(int x=0;x<exactMatches;x++) {

				// Pintamos las zonas de resultado
				nameNodeResultBall << resultBall << rowNumber  << "_" << totalBalls;
				Ogre::LogManager::getSingletonPtr()->logMessage("- " + nameNodeResultBall.str());
				SceneNode *nodeBall = sceneManager->createSceneNode(nameNodeResultBall.str());
				Entity *entHitZone = sceneManager->createEntity(nameNodeResultBall.str(), "Ball.mesh");
				entHitZone->setMaterialName("Balck");
				entHitZone->setQueryFlags(NOSELECTABLE);
				nodeBall->attachObject(entHitZone);
				nodeBall->setPosition(positions.at(totalBalls));
				nodeBall->scale(0.5,0.5,0.5);
				row->addChild(nodeBall);
				nameNodeResultBall.str("");
				// Fin zonas resultado
				totalBalls--;

	}

}

void DrawEntities::drawCombination(SceneManager* sceneManager)
{
	// Recuperamos el nodo de escenario
		SceneNode *head= sceneManager->getSceneNode("Head");

		// Pintamos las zonas calientes
		std::stringstream nameBall;
		string ball = "ball_";
		int splitBall = 2;
		int initBall = 0;
		for(int x=0;x<4;x++) {
			nameBall << ball << x ;
			Ogre::LogManager::getSingletonPtr()->logMessage("- " + nameBall.str());
			SceneNode *nodeBall = sceneManager->createSceneNode(nameBall.str());
			Entity *entBall = sceneManager->createEntity(nameBall.str(), "Ball.mesh");
			nodeBall->attachObject(entBall);
			entBall->setQueryFlags(NOSELECTABLE);
			nodeBall->setPosition(initBall-4,1,0);
			head->addChild(nodeBall);
			nameBall.str("");
			initBall += splitBall;
		}

}

void DrawEntities::showButton(int rowNumber,SceneManager* sceneManager)
{
	// Recuperamos la fila
	std::stringstream nameRow;
	string baseRow = "BaseBoard_";
	nameRow << baseRow << rowNumber ;
	SceneNode *row= sceneManager->getSceneNode(nameRow.str());

	if(!sceneManager->hasSceneNode("Button")){
		SceneNode *board = sceneManager->getSceneNode("Board");
		// Pintamos el botón de comprobación
		SceneNode *nodeButton = sceneManager->createSceneNode("Button");
		Entity *entButton = sceneManager->createEntity("Button", "Button.mesh");
		entButton->setQueryFlags(CHECKBUTTON);
		nodeButton->attachObject(entButton);
		board->addChild(nodeButton);
	}


	// Recuperamos botón
	SceneNode *button = sceneManager->getSceneNode("Button");
	// El padre anterior
	SceneNode *prevNode = button->getParentSceneNode();
	if(prevNode){
		prevNode->removeChild(button);
	}

	row->addChild(button);

	button->setPosition(6,0,0);

}

void DrawEntities::hideButton(SceneManager* sceneManager)
{


	// Recuperamos botón
	SceneNode *button = sceneManager->getSceneNode("Button");
	// El padre anterior
	SceneNode *prevNode = button->getParentSceneNode();
	if(prevNode){
		prevNode->removeChild(button);
	}

}

void DrawEntities::setColourInBoard(SceneManager* sceneManager, Colours::Colour * c, int rowNumber, int position){
	// Recuperamos la zona caliente que se ha pulsado gracias al nombre creado a partir e la fila y la posicion -> HitZone_xx_y
	ostringstream os1;

	if(rowNumber < 10)
		os1 << "HitZone_0" << rowNumber << "_" << position;
	else
		os1 << "HitZone_" << rowNumber << "_" << position;

	SceneNode * positionNode = sceneManager->getSceneNode(os1.str());
	SceneNode * placedBall;
	Ogre::SceneNode::ChildNodeIterator it = positionNode->getChildIterator();
	if(it.begin() != it.end()) {
		placedBall = (SceneNode*)it.getNext();
		((Entity*)placedBall->getAttachedObject(0))->setMaterialName(c->colorValue, "General");
	}
	else{
		ostringstream os2;

		if(rowNumber < 10)
			os2 << "PlacedBall_0" << rowNumber << "_" << position;
		else
			os2 << "PlacedBall_" << rowNumber << "_" << position;

		placedBall = sceneManager->createSceneNode(os2.str());
		Entity * placedBallEntity = sceneManager->createEntity(os2.str(),"Ball.mesh");
		placedBallEntity->setMaterialName(c->colorValue, "General");
		placedBallEntity->setQueryFlags(NOSELECTABLE);
		placedBall-> attachObject(placedBallEntity);

		positionNode->addChild(placedBall);
	}
}

void DrawEntities::drawAuthor(SceneManager* sceneManager)
{
	// Recuperamos el nodo del escenario
	SceneNode *room = sceneManager->getSceneNode("Room");

	// Pintamos el Tablero de autores
	SceneNode *nodeAuthor = sceneManager->createSceneNode("Author");
	Entity *entNodeAuthor = sceneManager->createEntity("Author", "Author.mesh");
	entNodeAuthor->setQueryFlags(NOSELECTABLE);
	nodeAuthor->attachObject(entNodeAuthor);
	nodeAuthor->setPosition(0.0,80.0,-80.0000);
	room->addChild(nodeAuthor);

	// Pintamos el botón AuthorExit
	SceneNode* nodeButtonExit = sceneManager->createSceneNode("AuthorExit");
	Entity *entButtonExit = sceneManager->createEntity("AuthorExit", "AuthorExit.mesh");
	nodeButtonExit->attachObject(entButtonExit);
	entButtonExit->setQueryFlags(EXITBUTTONS);
	nodeButtonExit->setPosition(28.0,62.0,-78.0);
	room->addChild(nodeButtonExit);
}

void DrawEntities::drawScoreBoard(SceneManager* sceneManager)
{
	// Recuperamos el nodo de escenario
	SceneNode *room= sceneManager->getSceneNode("Room");


	// Pintamos el tablero de records
	SceneNode *nodeScore = sceneManager->createSceneNode("Scoreboard");
	Entity *entNodeScore = sceneManager->createEntity("Scoreboard", "Scoreboard.mesh");
	entNodeScore->setQueryFlags(NOSELECTABLE);
	nodeScore->attachObject(entNodeScore);
	nodeScore->setPosition(-80.0000,80.0,0.0);
	room->addChild(nodeScore);

	//Pintamos el botón ScoreBoardExit
	SceneNode *nodeButtonExit2=sceneManager->createSceneNode("ScoreboardExit");
	Entity *entButtonExit2=sceneManager->createEntity("ScoreboardExit", "ScoreboardExit.mesh");
	nodeButtonExit2->attachObject(entButtonExit2);
	entButtonExit2->setQueryFlags(EXITBUTTONS);
	nodeButtonExit2->setPosition(-78.00001,52.0,-18.0);
	room->addChild(nodeButtonExit2);
}

void DrawEntities::setColourSelected(SceneManager* sceneManager, Colours::Colour * c, const Vector3 & position, const Vector3 & rayDirection) {
	// Si ya existia, primero lo borramos y luego asignamos el nuevo
	if(_colourSelected != NULL) emptyColourSelected(sceneManager);
	SceneNode * selectedBall = sceneManager->createSceneNode("ColourSelected");
	Entity * selectedBallEntity = sceneManager->createEntity("ColourSelected","Ball.mesh");
	selectedBallEntity->setMaterialName(c->colorValue, "General");
	selectedBallEntity->setQueryFlags(NOSELECTABLE);
	selectedBall-> attachObject(selectedBallEntity);

	OIS::Mouse * m = InputManager::getSingletonPtr()->getMouse();
	int posx = m->getMouseState().X.abs;
	int posy = m->getMouseState().Y.abs;
	selectedBall->setPosition(_x_o + ((_x_f - _x_o) * (posx/_width)) , 44, _y_o + ((_y_f - _y_o) * (posy/_height)));

	sceneManager->getRootSceneNode()->addChild(selectedBall);

	_colourSelected = selectedBall;
}

void DrawEntities::emptyColourSelected(SceneManager* sceneManager) {
	if(_colourSelected != NULL) {
		sceneManager->getSceneNode("SelectableBalls")->removeChild(_colourSelected);
		sceneManager->destroyEntity("ColourSelected");
		sceneManager->destroySceneNode(_colourSelected);

		//delete _colourSelected;
		_colourSelected = NULL;
	}
}

void DrawEntities::setBallSelectedPossibleCoordinates(Real x_o, Real x_f, Real y_o, Real y_f, int width, int height){
	_x_o = x_o; _x_f = x_f; _y_o = y_o, _y_f = y_f;
	_width = width; _height = height;
}


