#include "MessageState.h"

template<> MessageState* Ogre::Singleton<MessageState>::msSingleton = 0;

void
MessageState::enter ()
{
	switch (_type){
		case WIN:
			_scoreBoardLogic = new ScoreBoardLogic();
			_scoreBoardLogic->loadScores();
			createWinGUI();
			break;
		case LOSE:
			createLoseGUI();
			break;
		case LEVEL:
			createLevelGUI();
			break;
	}
}

void
MessageState::exit ()
{
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_window);
	_window->destroy();

}

void
MessageState::pause()
{
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_window);
	_window->destroy();
}

void
MessageState::resume()
{
}

void
MessageState::setType(MessageType type) {
	_type = type;
}

bool
MessageState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Real _timeSinceLastFrame = evt.timeSinceLastFrame;
  CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(_timeSinceLastFrame);
  return true;
}

bool
MessageState::frameEnded
(const Ogre::FrameEvent& evt)
{
	return true;
}

void
MessageState::keyPressed
(const OIS::KeyEvent &e)
{
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void
MessageState::keyReleased
(const OIS::KeyEvent &e)
{
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(e.key));
}

void
MessageState::mouseMoved
(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx,rely);
}

void
MessageState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}


void
MessageState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

MessageState*
MessageState::getSingletonPtr ()
{
return msSingleton;
}

MessageState&
MessageState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

CEGUI::MouseButton MessageState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void MessageState::createWinGUI(){
	//Config Window
	 _window = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("win.layout");

	//Back to menu
	  CEGUI::Window* exitButton = _window->getChild("Button");
	  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
					 CEGUI::Event::Subscriber(&MessageState::saveRecordCallback,
								  this));

	//sheet->addChild(loseWindow);
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_window);
}

void MessageState::createLoseGUI(){

	//Config Window
	 _window = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("lose.layout");

	//Back to menu
	  CEGUI::Window* exitButton = _window->getChild("FrameWindow")->getChild("MenuButton");
	  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
				     CEGUI::Event::Subscriber(&MessageState::menuButtonCallback,
							      this));

	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_window);
}

void MessageState::createLevelGUI(){
	//Config Window
	 _window = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("level.layout");

	//Back to menu
	  CEGUI::Window* button1 = _window->getChild("level1");
	  button1->subscribeEvent(CEGUI::PushButton::EventClicked,
					 CEGUI::Event::Subscriber(&MessageState::selectLevelCallback1,
								  this));

	  //Back to menu
	  	  CEGUI::Window* button2 = _window->getChild("level2");
	  	button2->subscribeEvent(CEGUI::PushButton::EventClicked,
	  					 CEGUI::Event::Subscriber(&MessageState::selectLevelCallback2,
	  								  this));

	  	//Back to menu
	  		  CEGUI::Window* button3 = _window->getChild("level3");
	  		button3->subscribeEvent(CEGUI::PushButton::EventClicked,
	  						 CEGUI::Event::Subscriber(&MessageState::selectLevelCallback3,
	  									  this));

	//sheet->addChild(loseWindow);
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_window);
}

void MessageState::menuButtonCallback(const CEGUI::EventArgs & e){
	changeState(IntroState::getSingletonPtr());
}


void MessageState::saveRecordCallback(const CEGUI::EventArgs & e){
	CEGUI::String Msg = _window->getChild("Editbox")->getText();
	_scoreBoardLogic->addNewScore(Msg.c_str(), PlayState::getSingletonPtr()->getScore());
	changeState(IntroState::getSingletonPtr());
}

void MessageState::selectLevelCallback1(const CEGUI::EventArgs & e){
	PlayState::getSingletonPtr()-> setRowNumber(15);
	changeState(PlayState::getSingletonPtr());
}
void MessageState::selectLevelCallback2(const CEGUI::EventArgs & e){
	PlayState::getSingletonPtr()-> setRowNumber(10);
	changeState(PlayState::getSingletonPtr());
}
void MessageState::selectLevelCallback3(const CEGUI::EventArgs & e){
	PlayState::getSingletonPtr()-> setRowNumber(5);
	changeState(PlayState::getSingletonPtr());
}
