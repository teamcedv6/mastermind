/*
 * ScoreBoardState.cpp
 *
 *  Created on: 10 dic. 2016
 *      Author: joe
 */
#include "ScoreBoardState.h"
#include "ScoreBoardLogic.h"
#include <OgreFont.h>
#include <OgreFontManager.h>

template<> ScoreBoardState* Ogre::Singleton<ScoreBoardState>::msSingleton = 0;

void ScoreBoardState::enter()
{
	_root=Ogre::Root::getSingletonPtr();

	_exitGame = false;

	// Recuperamos SceneManager y Cámara de GameManager
	_sceneMgr =_root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");

	// Cremos rayo de consulta
	_raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

	Vector3 p3 = Vector3(-41.5,72,-1.31);
	Quaternion q = Quaternion(1, 0, 1, 0);
	CameraAnimation * anim3 = new CameraAnimation(_camera, _root, 5.0);

	anim3-> setPosition(p3) -> setOrientation(q) -> setFOV(Radian(Degree(70))) -> startAnimation();
	_animTime=0;
}

void ScoreBoardState::exit()
{
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_window);
	_window->destroy();
}

void ScoreBoardState::pause()
{
}

void ScoreBoardState::resume()
{
}

void ScoreBoardState::keyPressed(const OIS::KeyEvent &e)
{
}

void ScoreBoardState::keyReleased(const OIS::KeyEvent &e)
{
	if (e.key == OIS::KC_ESCAPE)	{
			_exitGame=true;
	}
}

void ScoreBoardState::mouseMoved(const OIS::MouseEvent &e)
{
	int relx = e.state.X.rel;
	int rely = e.state.Y.rel;
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(relx,rely);
}

void ScoreBoardState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	Ogre::uint32 mask;

	if(id == OIS::MB_Left) {
		mask = EXITBUTTONS;

		Ogre::Ray r = setRayQuery(_raySceneQuery, _root->getAutoCreatedWindow(),_camera, posx, posy, mask);
		Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();

		Ogre::RaySceneQueryResult::iterator it;
		it = result.begin();

		if (it != result.end()) {
			if(it->movable->getParentNode()->getName()=="ScoreboardExit")
				popState();
		}
	}
}

void ScoreBoardState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

bool ScoreBoardState::frameStarted (const Ogre::FrameEvent& e)
{
	if(_animTime != -1){
		_animTime += e.timeSinceLastFrame;
		if(_animTime >= 5.0) {
			_animTime = -1;
			// Dibujar creditos con cegui
			ScoreBoardLogic * scoreBoardLogic =  new ScoreBoardLogic();
			std::vector < pair <string, int> > scores = scoreBoardLogic->loadScores();

			//Config Window
			 _window = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");

			 CEGUI::Window* title = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Label","Title");
			  title->setText("[colour='FF000000'][font='Jura-18']Scoreboard");
			  title->setSize(CEGUI::USize(CEGUI::UDim(0,200),CEGUI::UDim(0.0,20)));
			  title->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.5,-100),CEGUI::UDim(0,20)));
			_window->addChild(title);

			CEGUI::Window* recordFrame;
			CEGUI::Window* name;
			CEGUI::Window* score;
			for(int i = 0; i < scores.size(); i++) {
				ostringstream os1;
				os1 << i;
				recordFrame = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Frame"+os1.str());
				name = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Label","Name"+os1.str());
				score = CEGUI::WindowManager::getSingleton().createWindow("AlfiskoSkin/Label","Score"+os1.str());

				recordFrame ->setSize(CEGUI::USize(CEGUI::UDim(0,400),CEGUI::UDim(0.0,20)));
				recordFrame ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.5, -200),CEGUI::UDim(0, 60 + (i * 35))));

				name ->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(1.0,0)));
				name ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
				name -> setText("[colour='FF000000'][font='Jura-18']" + scores.at(i).first);

				ostringstream os2;
				os2 << "" << scores.at(i).second;
				score ->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(1.0,0)));
				score ->setPosition(CEGUI::Vector2<CEGUI::UDim>(CEGUI::UDim(0.7,0),CEGUI::UDim(0,0)));
				score -> setText("[colour='FF000000'][font='Jura-18']" + os2.str());

				recordFrame->addChild(name);
				recordFrame->addChild(score);

				_window-> addChild(recordFrame);
			}

			CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_window);

			delete scoreBoardLogic;
		}
	}


	return true;
}

bool ScoreBoardState::frameEnded (const Ogre::FrameEvent & e)
{
	if (_exitGame)
		return false;

	return true;
}

ScoreBoardState& ScoreBoardState::getSingleton()
{
	assert(msSingleton);
	return *msSingleton;
}

ScoreBoardState* ScoreBoardState::getSingletonPtr()
{
	return msSingleton;
}

void ScoreBoardState::WriteToTextureT(const String &str, TexturePtr destTexture, Image::Box destRectangle, Font* font, const ColourValue &color, char justify ,  bool wordwrap)
{

}

