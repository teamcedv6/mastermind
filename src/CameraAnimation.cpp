/*
 * CameraAnimation.cpp

 *
 *  Created on: Dec 4, 2016
 *      Author: root
 */


#include "CameraAnimation.h"
#include <string>
#include <ostream>

CameraAnimation::CameraAnimation(){}

CameraAnimation::CameraAnimation(Camera * cam, Root * root, Real time)
	: _cam(cam), _root(root),_t(time) {
	_position = false;
	_lookAt = false;
	_fov = false;
	_orientation = false;

	_initialPosition = NULL;
	_currentPosition = NULL;
	_positionChange = NULL;

	_initialLookAt = NULL;
	_currentLookAt = NULL;
	_lookAtChange = NULL;

	_initialfov = Radian(0);
	_currentfov = Radian(0);
	_fovChange = Radian(0);

	_elapsedTime = 0.0;

	_hasFinished = false;

}

CameraAnimation::CameraAnimation(Camera * cam, Root * root, Vector3 & position, Vector3 & lookAt, Real time)
	: _cam(cam), _root(root),_t(time) {

	_position = true;
	_lookAt = true;
	_fov = false;
	_orientation = false;

	_initialPosition = new Vector3(cam->getPosition());
	_currentPosition = new Vector3(cam->getPosition());
	_positionChange = new Vector3(position - *_currentPosition);

	_initialLookAt = new Vector3(cam->getPosition() + cam->getDirection() * 50);
	_currentLookAt = new Vector3(*_initialLookAt);
	_lookAtChange = new Vector3(lookAt - *_initialLookAt);

	_initialfov = Radian(0);
	_currentfov = Radian(0);
	_fovChange = Radian(0);

	_elapsedTime = 0.0;

	_hasFinished = false;
}

CameraAnimation* CameraAnimation::setPosition(Vector3 & position) {
	_position = true;

	_initialPosition = new Vector3(_cam->getPosition());
	_currentPosition = new Vector3(_cam->getPosition());
	_positionChange = new Vector3(position - *_currentPosition);

	return this;
}

CameraAnimation* CameraAnimation::setLookAt(Vector3 & lookAt) {
	_lookAt = true;

	_initialLookAt = new Vector3(_cam->getPosition() + _cam->getDirection() * 50);
	_currentLookAt = new Vector3(*_initialLookAt);
	_lookAtChange = new Vector3(lookAt - *_initialLookAt);

	return this;
}

CameraAnimation* CameraAnimation::setFOV(Radian fov) {
	_fov = true;

	_initialfov = Radian(_cam->getFOVy());
	_currentfov = Radian(_initialfov);
	_fovChange = fov - _initialfov;

	return this;
}

CameraAnimation* CameraAnimation::setOrientation(Quaternion & quaternion) {
	_orientation = true;

	_initialOrientation = Quaternion(_cam->getOrientation());
	_orientationQuaternion = quaternion;

	return this;
}

CameraAnimation::~CameraAnimation(){
	delete _currentPosition;
	delete _positionChange;
	delete _initialPosition;

	delete _currentLookAt;
	delete _initialLookAt;
	delete _lookAtChange;
}

bool CameraAnimation::frameStarted(const FrameEvent& evt){
	Real deltaT = evt.timeSinceLastFrame;
	_elapsedTime += deltaT;


	if(_position){
		easeFunctionVector(_currentPosition, _initialPosition, _positionChange);
		_cam->setPosition(*_currentPosition);
	}

	if(_lookAt) {
		easeFunctionVector(_currentLookAt, _initialLookAt, _lookAtChange);
		_cam->lookAt(*_currentLookAt);
	}

	if(_fov) {
		easeFunctionRadian(&_currentfov, _initialfov, _fovChange);
		/*std::ostringstream os;
		os << "if=" <<  _initialfov << " fc=" << _fovChange << " cf=" << _currentfov;
		Ogre::LogManager::getSingletonPtr()->logMessage("*** FocalLenght = " + os.str());*/
		_cam->setFOVy(_currentfov);
	}

	if(_orientation){
		//Quaternion currentOrientation = Quaternion::Slerp(_elapsedTime/_t, *_initialOrientation, *_orientationQuaternion, false);
		Quaternion currentOrientation;
		currentOrientation = Quaternion::Slerp(_elapsedTime/_t, _initialOrientation, _orientationQuaternion, true);
		_cam -> setOrientation(currentOrientation);
	}


	if(_elapsedTime >= _t) {
		_root->removeFrameListener(this);
		_hasFinished = true;
	}


	return true;
}

void CameraAnimation::easeFunctionVector(Vector3 * currentValue, Vector3 * startValue, Vector3 * valueChange){
	Real t = _elapsedTime;
	t = t / (_t/2);
	if (t < 1) {
		*currentValue = *valueChange * 0.5 * t * t + *startValue;
	}
	else {
		t--;
		*currentValue = -*valueChange * 0.5 * (t*(t-2) -1) + *startValue;
	}
}

void CameraAnimation::easeFunctionFloat(float * currentValue, float startValue, float valueChange) {
	Real t = _elapsedTime;
	t = t / (_t/2);
	if (t < 1) {
		*currentValue = valueChange * 0.5 * t * t + startValue;
	}
	else {
		t--;
		*currentValue = -valueChange * 0.5 * (t*(t-2) -1) + startValue;
	}
}

void CameraAnimation::easeFunctionRadian(Radian * currentValue, Radian startValue, Radian valueChange) {
	Real t = _elapsedTime;
	t = t / (_t/2);
	if (t < 1) {
		*currentValue = valueChange * 0.5 * t * t + startValue;
	}
	else {
		t--;
		*currentValue = -valueChange * 0.5 * (t*(t-2) -1) + startValue;
	}
}

bool CameraAnimation::frameEnded(const FrameEvent& evt){
	return true;
}

void CameraAnimation::startAnimation() {
	_root->addFrameListener(this);
}

bool CameraAnimation::hasFinished(){ return _hasFinished; }



